//
//  ViewController.swift
//  BigDesignComplexUI
//
//  Created by Enrique on 9/4/17.
//  Copyright © 2017 werewolf. All rights reserved.
//

import UIKit
import BigDesignUICatalog
import BigDesignManager

public enum BDENewDashboardSections : Int {
    case none
    case welcome
    case country
    case loading
}

public class BDENewDashboardSectionObject
{
    public var section : BDENewDashboardSections! = .none
    public var table : BDETableView?
    public var errorSectionType : BDENewDashboardSections!
    
    public init(section : BDENewDashboardSections, table: BDETableView?, errorSectionType : BDENewDashboardSections = .none)
    {
        self.section = section
        self.table = table
        self.errorSectionType = errorSectionType
    }
}


class BDEViewController: UIViewController {

    
    //Tabs
    var firstAppearance : Bool = false
    
    let duration : TimeInterval = (27/TimeInterval(30))
    //Main component
    @IBOutlet weak var dashboardView: UICollectionView!

    var sections : [BDENewDashboardSectionObject] = []
    
    public var userInfo : UserInfo?
    public var continentsInfo : [String : ContinentInfo] = [:]
    
    //welcome and operations
    var menuHeaderView : BDENewDashboardHeaderMenu!
    // Extra views
    var backgroundImage: UIImageView!
    var status : UIView!
    //animation
    
    @IBOutlet weak var animationBackgroundView : UIView!
    @IBOutlet weak var animationHeaderView : UIView!
    var canAnimate : Bool = true
    var lastAnimatedSection : Int = 2
    var canClean : Bool = false
    let headerSection : Int = 1
    let firstElementSection : Int = 2
    let bottomMarginAnimationName : String = "bottomMarginAnimation"
    let animationBackgroundTopName : String = "animationBackgroundTop"
    let animationHeaderTopName : String = "animationHeaderTop"
    var isHeaderHidden : Bool = false
    let constant : CGFloat = 50

    var isPopulating : Bool = false
    var isLoadingFinished : Bool = false
    required public init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        initialize()
    }
    
    override public func viewDidLayoutSubviews() {
        
        super.viewDidLayoutSubviews()
        
        if (self.menuHeaderView != nil)
        {
            self.menuHeaderView.layoutSubviews()
        }
    }
    
    override public init(nibName: String?, bundle: Bundle?)
    {
        super.init(nibName: nibName, bundle: bundle)
        initialize()
    }
    
    func initialize()
    {
        sections.append(BDENewDashboardSectionObject(section: .welcome, table: nil))
        sections.append(BDENewDashboardSectionObject(section: .none, table: nil))
        sections.append(BDENewDashboardSectionObject(section: .none, table: nil))
    }
    
    func createLoadingTable() -> BDELoadingTableView
    {
        var table : BDELoadingTableView
        table = BDELoadingTableView(animate: false)
        table.dashboard = self
        table.products = ["1" as AnyObject, "2" as AnyObject]
        table.reloadData()
        return table
    }
    
    func initialSettings()
    {
        self.view.backgroundColor = UIColor.clear//UIColor.BBVA100()//colorBasedOnTime()
        self.dashboardView.backgroundColor = UIColor.clear
        status = UIView (frame: CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: 20))
        status.backgroundColor = UIColor.white
        self.view.addSubview(status)

        self.animationBackgroundView.viewWithTag(headerSection)!.constraints.filter({$0.firstAttribute == .height }).first!.constant = UIScreen.main.bounds.size.height
        self.animationBackgroundView.viewWithTag(firstElementSection)!.constraints.filter({$0.firstAttribute == .height }).first!.constant = BDETableViewHeights.loadingView1
        
        //        animateBackgroundView(height: UIScreen.main.bounds.size.height)
    }
    
    
    
    func animateBackgroundView(height : CGFloat)
    {
        if (self.view.constraints.filter({ $0.identifier == bottomMarginAnimationName }).first != nil)
        {
            self.view.constraints.filter({ $0.identifier == bottomMarginAnimationName }).first!.constant = UIScreen.main.bounds.size.height - height
            
        }
    }
    
    func firstAnimation()
    {
        
        self.animationBackgroundView.viewWithTag(self.headerSection)!.backgroundColor = UIColor.BDEMorningBlue()
        self.animationBackgroundView.viewWithTag(self.firstElementSection)!.backgroundColor = UIColor.BDEMorningBlue()
        self.status.backgroundColor = UIColor.BDENavy()
        
        
        self.view.backgroundColor = UIColor.white
        self.animationHeaderView.backgroundColor = UIColor.BDEMorningBlue()
        self.animationBackgroundView.viewWithTag(self.headerSection)!.constraints.filter({$0.firstAttribute == .height }).first!.constant = BDETableViewHeights.DAHSBOARD_HEADER_NOT_LOADED
        
        self.animateBackgroundView(height: BDETableViewHeights.DAHSBOARD_HEADER_NOT_LOADED + BDETableViewHeights.loadingView1)
        if (self.menuHeaderView != nil)
        {
            self.menuHeaderView.animateButtonTransform()
        }

        self.view.layoutIfNeeded()
        self.animationBackgroundView.layoutIfNeeded()
        self.sections[1].section = BDENewDashboardSections.loading
        self.sections[1].table = self.createLoadingTable()
        self.sections[2].section = BDENewDashboardSections.loading
        self.sections[2].table = self.createLoadingTable()
        self.dashboardView.reloadData()
        
        self.canClean = true
        self.startLoadingProcess()
        self.canAnimate = false
        
        
    }

    private func registerCells()
    {
        self.dashboardView.register(BDEContainerCollectionViewCell.self, forCellWithReuseIdentifier: "default")
        
        self.dashboardView.register(BDEContainerCollectionReusableView.self, forSupplementaryViewOfKind: UICollectionElementKindSectionHeader, withReuseIdentifier: "header")
        
        self.dashboardView.register(BDEContainerCollectionReusableView.self, forSupplementaryViewOfKind: UICollectionElementKindSectionFooter, withReuseIdentifier: "footer")
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        registerCells()
        BDECardView.isDone = false
        initialSettings()
        // Do any additional setup after loading the view, typically from a nib.
    }
    
    override public func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if (!self.firstAppearance)
        {
            
            firstAppearance = true
            firstAnimation()
            
        }
        
        //self.navigationController!.isNavigationBarHidden = true
    }
    
    func startLoadingProcess()
    {
        
        self.dashboardView.isScrollEnabled = false
        //        NotificationCenter.default.addObserver(self, selector: #selector(self.reloadTable(notification:)), name: NSNotification.Name(rawValue: BGMCardView.kcBGMCardViewDoneNotification), object: nil)
        
        
       BigDesignManager.shared.getWorldInfo {
            
        
            self.isLoadingFinished = true
            
        
            DispatchQueue.main.async {
                if (!self.isPopulating)
                {
                    self.reloadTable(notification: nil)
                }
            }
            
        }
        
    }

    func reloadTable(notification: Notification?)
    {
        print("hello")
        self.dashboardView.isScrollEnabled = true
        
        self.populateData()

        self.isPopulating = true
    }

    func populateData()
    {
        if (self.isPopulating)
        {
            return
        }
        
        BDECardView.isDone = true
        var sectTep :  [BDENewDashboardSectionObject] = []
        
        sectTep.append(BDENewDashboardSectionObject(section: .welcome, table: nil))
        
        for continent in BigDesignManager.shared.continentsInfo
        {
            let continentTable : BDEContryTableView = BDEContryTableView()
            continentTable.products  = continent.value.countries
            continentTable.dashboard = self
            continentTable.continent = continent.value.continent
            continentTable.reloadData()
            sectTep.append(BDENewDashboardSectionObject(section: .country, table: continentTable))
        }
        
        self.menuHeaderView.operations = BigDesignManager.shared.userInfo!.actions
        //Animation constraints////////
        
        let height : CGFloat = sectTep[1].table!.height
        let headerHeight : CGFloat = self.menuHeaderView.operations!.count > 0 ? BDETableViewHeights.DAHSBOARD_HEADER : BDETableViewHeights.DAHSBOARD_HEADER_NOT_OPERATIONS
        
        self.animationBackgroundView.viewWithTag(self.headerSection)!.constraints.filter({$0.firstAttribute == .height }).first!.constant = headerHeight
        
        self.animationBackgroundView.viewWithTag(self.firstElementSection)!.constraints.filter({$0.firstAttribute == .height }).first!.constant = height - self.constant
        
        self.animateBackgroundView(height: headerHeight + height)
        self.view.layoutIfNeeded()
        /////////
        var frame : CGRect = self.menuHeaderView.frame
        frame.size.height = headerHeight
        self.menuHeaderView.frame = frame
        self.menuHeaderView.setNeedsDisplay()
        self.menuHeaderView.operationsContainer.layoutIfNeeded()
        (self.animationBackgroundView.viewWithTag(self.firstElementSection) as! BDETriangleView).animate = false
        self.animationBackgroundView.viewWithTag(self.firstElementSection)!.setNeedsDisplay()
        
        self.animationBackgroundView.layoutIfNeeded()
        
        
        self.sections = sectTep
        
        self.dashboardView.reloadData()
        self.view.viewWithTag(100)!.removeFromSuperview()
    }
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

extension BDEViewController : UICollectionViewDataSource
{
    public func numberOfSections(in collectionView: UICollectionView) -> Int {
        return sections.count
    }
    
    public func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int
    {
        return 1
    }
    
    public func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell : UICollectionViewCell = collectionView.dequeueReusableCell(withReuseIdentifier: "default", for: indexPath)
        
        return cell
    }
    
    public func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        if (kind == UICollectionElementKindSectionFooter)
        {
            return collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: "footer", for: indexPath)
        }
        else {
            return collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: "header", for: indexPath)
        }
        
    }


}

extension BDEViewController : UICollectionViewDelegate
{
    public func collectionView(_ collectionView: UICollectionView, willDisplaySupplementaryView view: UICollectionReusableView, forElementKind elementKind: String, at indexPath: IndexPath) {
        if (elementKind == UICollectionElementKindSectionFooter)
        {
            //view.backgroundColor = UIColor.BBVA100()
        }
    }
    
    public func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        
        var subView: UIView
        if (cell.viewWithTag(1) != nil)
        {
            subView = cell.viewWithTag(1)!
            subView.subviews.forEach({ (vi) in
                vi.removeFromSuperview()
            })
            subView.frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: cell.bounds.height)
            subView.layer.sublayers?.forEach({ (layer) in
                layer.removeFromSuperlayer()
            })
        }
        else
        {
            subView = UIView(frame: CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: cell.bounds.height))
            subView.backgroundColor = UIColor.clear//UIColor.BBVA100()
            subView.tag = 1
            subView.isUserInteractionEnabled = true
            cell.contentView.addSubview(subView)
        }
        
        if (self.sections[indexPath.section].section == BDENewDashboardSections.welcome)
        {
            if (menuHeaderView == nil)
            {
                let bundle: Bundle = Bundle(for: BDENewDashboardHeaderMenu.self)
                menuHeaderView = bundle.loadNibNamed("BDENewDashboardHeaderMenu", owner: nil, options: nil)![0] as! BDENewDashboardHeaderMenu
                
                menuHeaderView.dashboard = self
                menuHeaderView.setUserInfo(user: BigDesignManager.shared.userInfo!.user, animate: false)
                menuHeaderView.tag = 2
            }
            menuHeaderView.frame = cell.frame
            subView.addSubview(menuHeaderView)
            menuHeaderView.layoutSubviews()
        }
            
        else if (self.sections[indexPath.section].table != nil)
        {
            var frame : CGRect = self.sections[indexPath.section].table!.frame
            frame.origin.x = 10
            frame.size.width = UIScreen.main.bounds.width - 20
            frame.size.height = self.sections[indexPath.section].table!.height
            
            self.sections[indexPath.section].table!.frame = frame
            let viewV : UIView = UIView(frame: frame)
            
            prepareTable(table: self.sections[indexPath.section].table!)
            prepareShadow(viewV: viewV)
            
            subView.addSubview(viewV)
            subView.addSubview(self.sections[indexPath.section].table!)
            
        }
        
        
    }
    
    func prepareTable(table : BDETableView)
    {
        table.layer.borderWidth = 1
        table.layer.borderColor = UIColor.clear.cgColor
        table.layer.cornerRadius = 2
    }
    
    func prepareShadow(viewV : UIView)
    {
        viewV.layer.borderWidth = 1
        viewV.backgroundColor = UIColor.white
        viewV.layer.borderColor = UIColor.clear.cgColor
        viewV.layer.cornerRadius = 2
        viewV.layer.shadowOffset = CGSize(width: 0, height: 1)
        viewV.layer.shadowRadius = 3
        viewV.layer.shadowOpacity = 1
        viewV.layer.shadowColor = UIColor.black.withAlphaComponent(0.15).cgColor
        viewV.layer.masksToBounds = false
        viewV.clipsToBounds = false
        viewV.tag = 2
    }
}

extension BDEViewController : UICollectionViewDelegateFlowLayout
{
    public func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize
    {
        
        if (self.sections[indexPath.section].section == BDENewDashboardSections.none)
        {
            return CGSize.zero
        }
        else if (self.sections[indexPath.section].section == BDENewDashboardSections.welcome)
        {
            var height : CGFloat = 0
            
            if (self.canClean && BigDesignManager.shared.userInfo!.actions.count > 0)
            {
                height = BDETableViewHeights.DAHSBOARD_HEADER
            }
            else if (self.canClean) {
                height = BDETableViewHeights.DAHSBOARD_HEADER_NOT_OPERATIONS
            }
            else
            {
                height = BDETableViewHeights.DAHSBOARD_HEADER_NOT_LOADED
            }
            
            return CGSize(width: UIScreen.main.bounds.width, height: height)// + BGMDashboardHeights.footer)
        }
        
        else
        {
            return CGSize(width: UIScreen.main.bounds.width, height: self.sections[indexPath.section].table != nil ? self.sections[indexPath.section].table!.height : 0)
        }
    }
    
    public func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
        return CGSize(width: UIScreen.main.bounds.width, height: 0)
    }
    
    public func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForFooterInSection section: Int) -> CGSize {
        if (section != 0)
        {
            if (self.sections[section].section == BDENewDashboardSections.none)
            {
                return CGSize.zero
            }
            return CGSize(width: UIScreen.main.bounds.width, height: BDETableViewHeights.footer)
        }
        
        return CGSize(width: UIScreen.main.bounds.width, height: 0)
        
    }
    
    
}
