//
//  BigDesignManager.swift
//  BigDesignManager
//
//  Created by Enrique on 9/6/17.
//  Copyright © 2017 werewolf. All rights reserved.
//

import UIKit

public class BigDesignManager
{
    private var bundle : Bundle = Bundle(for: BigDesignManager.self)
    
    public var userInfo : UserInfo?
    public var continentsInfo : [String : ContinentInfo] = [:]
    
    public static let shared : BigDesignManager = BigDesignManager()
    
    let continents : DispatchQueue = DispatchQueue(label: "continents", qos: DispatchQoS.utility, attributes: [.concurrent], autoreleaseFrequency: .inherit, target: nil)
    let semaphore : DispatchSemaphore = DispatchSemaphore(value: 1)
    
    private init()
    {
        getUserInfo()
    }
    
    private func readfile(name : String ) -> String?
    {
        if let path : String = self.bundle.path(forResource: name, ofType: "json", inDirectory: nil, forLocalization: nil)
        {
            var file : String
            do
            {
                file = try String(contentsOfFile: path)
                return file
                
            }
            catch
            {
            }
            
        }
        return nil
    }
    
    private func getContinentInfo(continentName: String, onFinish: @escaping (_ userInfo: ContinentInfo?) -> Void)
    {
        let diceRoll = Int(arc4random_uniform(2) + 1)
        continents.asyncAfter(deadline: .now() + .seconds(diceRoll)) {
            
            if let file : String = self.readfile(name: continentName)
            {
                if let data = file.data(using: String.Encoding.utf8)
                {
                    do
                    {
                        self.semaphore.wait()
                        let obj : [String:Any] = try JSONSerialization.jsonObject(with: data, options: []) as! [String:Any]
                        
                        self.continentsInfo[continentName] = ContinentInfo(dic: obj)
                        onFinish(self.continentsInfo[continentName])
                        
                        self.semaphore.signal()
                        
                    }
                    catch
                    {
                        
                    }
                }
            }
        }
    }
    
    public func getWorldInfo(onFinish: @escaping () -> Void)
    {
        let group : DispatchGroup = DispatchGroup()
        
        
        group.enter()
        self.getEuropeInfo { (continent) in
            group.leave()
        }
        
        group.enter()
        self.getAfricaInfo { (continent) in
            group.leave()
        }
        
        group.enter()
        self.getAsiaInfo { (continent) in
            group.leave()
        }
        
        group.enter()
        self.getAmericaInfo { (continent) in
            group.leave()
        }
        
        group.enter()
        self.getOceaniaInfo { (continent) in
            group.leave()
        }

        group.notify(queue: .main) { 
            onFinish()
        }
    }
    
    public func getEuropeInfo(onFinish: @escaping (_ userInfo: ContinentInfo?) -> Void)
    {
        self.getContinentInfo(continentName: "Europe", onFinish: onFinish)
    }
    
    public func getAfricaInfo(onFinish: @escaping (_ userInfo: ContinentInfo?) -> Void)
    {
        self.getContinentInfo(continentName: "Africa", onFinish: onFinish)
    }
    
    public func getAsiaInfo(onFinish: @escaping (_ userInfo: ContinentInfo?) -> Void)
    {
        self.getContinentInfo(continentName: "Asia", onFinish: onFinish)
    }
    
    public func getAmericaInfo(onFinish: @escaping (_ userInfo: ContinentInfo?) -> Void)
    {
        self.getContinentInfo(continentName: "America", onFinish: onFinish)
    }
    
    public func getOceaniaInfo(onFinish: @escaping (_ userInfo: ContinentInfo?) -> Void)
    {
        self.getContinentInfo(continentName: "Oceania", onFinish: onFinish)
    }
    
    public func getUserInfo()
    {
        if let file : String = self.readfile(name: "User")
        {
            if let data = file.data(using: String.Encoding.utf8)
            {
                do
                {
                    let obj : [String:Any] = try JSONSerialization.jsonObject(with: data, options: []) as! [String:Any]
                    
                    self.userInfo = UserInfo(dic: obj)
                    
                    
                }
                catch
                {
                    
                }
            }
        }
    }
}
