//
//  ContinentInfo.swift
//  BigDesignManager
//
//  Created by Enrique on 9/6/17.
//  Copyright © 2017 werewolf. All rights reserved.
//

import Foundation
public class ContinentInfo
{
    public var continent: Continent!
    public var countries: [Country]!
    
    public init(dic: [String : Any])
    {
        let continentDic : [String : Any] = dic["continent"] as! [String : Any]
        self.continent = Continent()
        self.continent.id = continentDic["id"] as! String
        self.continent.name = continentDic["name"] as! String
        self.countries = []
        for country in dic["countries"] as! [Any]
        {
            let countryDic : [String : Any] = country as! [String : Any]
            let countryObj : Country = Country()
            countryObj.id = countryDic["id"] as! String
            countryObj.name = countryDic["name"] as! String
            countryObj.capital = countryDic["Capital"] as! String
            countryObj.population = countryDic["Population"] as! Int
            self.countries.append(countryObj)
        }
    }
}
