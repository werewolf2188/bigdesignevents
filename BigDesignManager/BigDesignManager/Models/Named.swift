//
//  Named.swift
//  BigDesignManager
//
//  Created by Enrique on 9/6/17.
//  Copyright © 2017 werewolf. All rights reserved.
//

import Foundation

public protocol Named
{
    var id : String! { get set }
    var name : String! { get set }
}
