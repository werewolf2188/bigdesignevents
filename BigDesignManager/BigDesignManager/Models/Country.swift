//
//  Country.swift
//  BigDesignManager
//
//  Created by Enrique on 9/6/17.
//  Copyright © 2017 werewolf. All rights reserved.
//

import Foundation
public class Country : Named
{
    private var _id : String!
    
    public var id : String! {
        get
        {
            return _id
        }
        set
        {
            _id = newValue
        }
    }
    private var _name : String!
    
    public var name : String! {
        get
        {
            return _name
        }
        set
        {
            _name = newValue
        }
    }
    
    private var _capital : String!
    
    public var capital : String! {
        get
        {
            return _capital
        }
        set
        {
            _capital = newValue
        }
    }
    
    private var _population : Int!
    
    public var population : Int! {
        get
        {
            return _population
        }
        set
        {
            _population = newValue
        }
    }
    
}
