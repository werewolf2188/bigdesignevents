//
//  User.swift
//  BigDesignManager
//
//  Created by Enrique on 9/6/17.
//  Copyright © 2017 werewolf. All rights reserved.
//

import Foundation

public class User
{
    private var _firstName : String!
    
    public var firstName : String! {
        get
        {
            return _firstName
        }
        set
        {
            _firstName = newValue
        }
    }
    private var _lastName : String!
    
    public var lastName : String! {
        get
        {
            return _lastName
        }
        set
        {
            _lastName = newValue
        }
    }

}
