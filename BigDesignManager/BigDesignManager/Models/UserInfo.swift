//
//  UserInfo.swift
//  BigDesignManager
//
//  Created by Enrique on 9/6/17.
//  Copyright © 2017 werewolf. All rights reserved.
//

import Foundation

public class UserInfo
{
    public var user: User!
    public var actions: [Action]!
    
    public init(dic: [String : Any])
    {
        let userDic : [String : Any] = dic["user"] as! [String : Any]
        self.user = User()
        self.user.firstName = userDic["firstName"] as! String
        self.user.lastName = userDic["lastName"] as! String
        self.actions = []
        for action in dic["actions"] as! [Any]
        {
            let actionDic : [String : Any] = action as! [String : Any]
            let actionObj : Action = Action()
            actionObj.id = actionDic["id"] as! String
            actionObj.name = actionDic["name"] as! String
            self.actions.append(actionObj)
        }
    }
}
