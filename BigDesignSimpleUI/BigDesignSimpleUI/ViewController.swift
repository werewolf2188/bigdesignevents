//
//  ViewController.swift
//  BigDesignSimpleUI
//
//  Created by Enrique on 9/4/17.
//  Copyright © 2017 werewolf. All rights reserved.
//

import UIKit
import BigDesignManager
class ViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {

    @IBOutlet weak var tableView: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        BigDesignManager.shared.getWorldInfo {
            let headerView : UIView = Bundle(for: ViewController.self).loadNibNamed("HeaderView", owner: nil, options: nil)?.first! as! UIView
            
            
            (headerView.viewWithTag(1) as! UILabel).text = "Hello \(BigDesignManager.shared.userInfo!.user!.firstName!) \(BigDesignManager.shared.userInfo!.user!.lastName!)"
            
            (headerView.viewWithTag(2) as! UIButton).setTitle(BigDesignManager.shared.userInfo!.actions[0].name, for: UIControlState.normal)
            (headerView.viewWithTag(3) as! UIButton).setTitle(BigDesignManager.shared.userInfo!.actions[1].name, for: UIControlState.normal)
            (headerView.viewWithTag(4) as! UIButton).setTitle(BigDesignManager.shared.userInfo!.actions[2].name, for: UIControlState.normal)
            (headerView.viewWithTag(5) as! UIButton).setTitle(BigDesignManager.shared.userInfo!.actions[3].name, for: UIControlState.normal)
            self.tableView.tableHeaderView = headerView
            self.tableView.reloadData()
        }
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


    func numberOfSections(in tableView: UITableView) -> Int {
        
        return BigDesignManager.shared.continentsInfo.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        let firstKey : String = Array(BigDesignManager.shared.continentsInfo.keys)[section]
        return BigDesignManager.shared.continentsInfo[firstKey] != nil ? BigDesignManager.shared.continentsInfo[firstKey]!.countries.count : 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell : UITableViewCell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
        
        let firstKey : String = Array(BigDesignManager.shared.continentsInfo.keys)[indexPath.section]
        (cell.viewWithTag(1) as! UILabel).text = "Country: \(BigDesignManager.shared.continentsInfo[firstKey]!.countries[indexPath.row].name!)"
        (cell.viewWithTag(2) as! UILabel).text = "Capital: \(BigDesignManager.shared.continentsInfo[firstKey]!.countries[indexPath.row].capital!)"
        (cell.viewWithTag(3) as! UILabel).text = "Population: \(BigDesignManager.shared.continentsInfo[firstKey]!.countries[indexPath.row].population!)"
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        let firstKey : String = Array(BigDesignManager.shared.continentsInfo.keys)[section]
        return BigDesignManager.shared.continentsInfo[firstKey] != nil ? BigDesignManager.shared.continentsInfo[firstKey]!.continent.name : ""
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80
    }
}

