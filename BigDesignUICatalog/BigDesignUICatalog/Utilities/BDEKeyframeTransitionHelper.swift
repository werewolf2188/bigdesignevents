//
//  BDEKeyframeTransitionHelper.swift
//  BigDesignUICatalog
//
//  Created by Enrique on 9/12/17.
//  Copyright © 2017 werewolf. All rights reserved.
//

import Foundation
//
//  BGMKeyframeTransitionHelper.swift
//  BGMCoreUISDKIOS
//
//  Created by Enrique Ricalde on 2/3/17.
//  Copyright © 2017 BBVA. All rights reserved.
//

import UIKit

/// Global value to make the amount of key frames per second in the animation
let keyframes : Int = 15

/// Helper class for making transitions using key frames, based on controlling values in ease out or ease curve.
public class BDEKeyframeTransitionHelper: NSObject {
    
    
    /**
     Closure to make an ease out cubic curve based on the on a final value
     
     - Parameter p: The value to be incremented/decremented.
     
     - Returns: The value transformed.
     */
    public let easeOutCubic : (_ p: CGFloat) -> CGFloat = { (_ p: CGFloat) in
        let f : CGFloat = (p - 1)
        return f * f * f + 1
    }
    
    /**
     Closure to make an ease out quad curve based on the on a final value
     
     - Parameter p: The value to be incremented/decremented.
     
     - Returns: The value transformed.
     */
    public let easeOutQuad : (_ p: CGFloat) -> CGFloat = { (_ p: CGFloat) in
        return -(p * (p - 2))
    }
    
    /**
     Closure to make an ease in cubic curve based on the on a final value
     
     - Parameter p: The value to be incremented/decremented.
     
     - Returns: The value transformed.
     */
    public let easeInCubic : (_ p: CGFloat) -> CGFloat = { (_ p: CGFloat) in
        return p * p * p
    }
    
    /**
     Closure to make an ease in quad curve based on the on a final value
     
     - Parameter p: The value to be incremented/decremented.
     
     - Returns: The value transformed.
     */
    public let easeInQuad : (_ p: CGFloat) -> CGFloat = { (_ p: CGFloat) in
        return p * p
    }
    
    /**
     Function that makes the key frame animation based on the initial and final value and the curve asked for the interval of the values.
     
     - Parameters:
     - duration: The duration of the animation
     - keyframesCount: The number of key frames
     - startValue: The starting value
     - endValue: The ending value
     - functionUsed: The function that will be manipulating the value between start and end
     - value: The value to pass to the closure functionUsed
     - animationFrame: The function used to tell the animation, what to do with the values
     - frame: The number of the frame
     - value: The value at that time
     */
    public func animationWithKeyFrame(duration: TimeInterval, keyframesCount : Int, startValue: CGFloat, endValue: CGFloat, functionUsed: @escaping (_ value: CGFloat) -> CGFloat, animationFrame: @escaping (_ frame: Int, _ value: CGFloat) -> Void)
    {
        let fromValue : CGFloat = startValue
        let toValue : CGFloat = endValue
        
        var t : Double = 0
        let dt : Double = 1.0 / (Double(keyframesCount) - 1);
        var frame : Int = 0
        var value : CGFloat = 0
        
        while frame <= keyframesCount
        {
            value = CGFloat(fromValue) + functionUsed(CGFloat(t)) * (CGFloat(toValue) - CGFloat(fromValue))
            UIView.addKeyframe(withRelativeStartTime: Double(t), relativeDuration: Double(dt), animations: {
                
                animationFrame(frame, value)
            })
            t += dt
            frame += 1
        }
    }
}
