//
//  BDEUtilities.swift
//  BigDesignUICatalog
//
//  Created by Enrique on 9/9/17.
//  Copyright © 2017 werewolf. All rights reserved.
//

import Foundation

public class BDETableViewHeights
{
    static public let header : CGFloat = 53
    static public let loadingView1 : CGFloat = 75
    static public let country : CGFloat = 83
    static public let footer : CGFloat = 10
    
    static public let DAHSBOARD_HEADER : CGFloat = 280
    static public let DAHSBOARD_HEADER_NOT_LOADED: CGFloat = 245
    static public let DAHSBOARD_HEADER_NOT_OPERATIONS : CGFloat = 150
}
