//
//  BGMExtension-Int.swift
//  BGMCoreUISDKIOS
//
//  Created by Pradeep Gangabhathina on 3/17/17.
//  Copyright © 2017 BBVA. All rights reserved.
//

import UIKit
import Foundation

public extension Int {
    
    var degreesToRadians: Double { return Double(self) * .pi / 180 }
    var radiansToDegrees: Double { return Double(self) * 180 / .pi }
    
}
