//
//  BGMBDEColor.swift
//  BGMCoreUISDKIOS
//
//  Created by Manish Sama on 1/23/17.
//  Copyright © 2017 BDE. All rights reserved.
//

import UIKit

public extension UIColor {
    
    public convenience init(red: Int, green: Int, blue: Int) {
        assert(red >= 0 && red <= 255, "Invalid red component")
        assert(green >= 0 && green <= 255, "Invalid green component")
        assert(blue >= 0 && blue <= 255, "Invalid blue component")
        
        self.init(red: CGFloat(red) / 255.0, green: CGFloat(green) / 255.0, blue: CGFloat(blue) / 255.0, alpha: 1.0)
    }
    
    public func hexString(_ includeAlpha: Bool = true) -> String {
        var r: CGFloat = 0
        var g: CGFloat = 0
        var b: CGFloat = 0
        var a: CGFloat = 0
        self.getRed(&r, green: &g, blue: &b, alpha: &a)
        
        if (includeAlpha) {
            return String(format: "0x%02X%02X%02X%02X", Int(r * 255), Int(g * 255), Int(b * 255), Int(a * 255))
        } else {
            return String(format: "0x%02X%02X%02X", Int(r * 255), Int(g * 255), Int(b * 255))
        }
    }
    
    public class func bdeBlue() -> UIColor {
        return UIColor(red:0.03, green:0.13, blue:0.27, alpha:1.0)
    }
    
    public convenience init(netHex:Int) {
        self.init(red:(netHex >> 16) & 0xff, green:(netHex >> 8) & 0xff, blue:netHex & 0xff)
    }
    
    public class func BDENavy() -> UIColor {
        return UIColor(netHex: 0x072146)
    }
    
    public class func BDECoreBlue() -> UIColor {
        return UIColor(netHex: 0x004481)
    }
	
    public class func BDEDarkCoreBlue() -> UIColor {
        return UIColor(netHex: 0x043263)
    }
    
    public class func BDEMediumBlue() -> UIColor {
        return UIColor(netHex: 0x2A86CA)
    }
    
    public class func BDEMorningBlue() -> UIColor
    {
        return UIColor(netHex: 0x237ABA)//
    }
    
    public class func BDEWhiteMediumBlue() -> UIColor {
        return UIColor(netHex: 0x49A5E6)
    }
    
    public class func BDEDarkMediumBlue() -> UIColor {
        return UIColor(netHex: 0x1464A5)
    }
    
    public class func BDEDarkAfternoonBlue() -> UIColor
    {
        return UIColor(netHex: 0x00396C)
    }
    
    public class func BDEDarkEveningBlue() -> UIColor
    {
        return UIColor(netHex: 0x00142F)
    }
    
    public class func BDELightBlue() -> UIColor {
        return UIColor(netHex: 0x5BBEFF)
    }
    
    public class func BDEWhiteLightBlue() -> UIColor {
        return UIColor(netHex: 0xD4EDFC)
    }
    
    public class func BDEDarkLightBlue() -> UIColor {
        return UIColor(netHex: 0x49A5E6)
    }
    
    public class func BDEAqua() -> UIColor {
        return UIColor(netHex: 0x2dcccd)
    }
    
    public class func BDEWhiteAqua() -> UIColor {
        return UIColor(netHex: 0xeaf9fa)
    }
    
    public class func BDELightAqua() -> UIColor {
        return UIColor(netHex: 0x5ac4c4)
    }
    
    public class func BDEDarkAqua() -> UIColor {
        return UIColor(netHex: 0x02a5a5)
    }
    
    public class func BDE600() -> UIColor {
        return UIColor(netHex: 0x121212)
    }
    
    public class func BDEMediumGold() -> UIColor{
        return UIColor(netHex: 0xB89F59)
    }
    
    public class func BDE500() -> UIColor {
        return UIColor(netHex: 0x666666)
    }
    
    public class func BDE400() -> UIColor {
        return UIColor(netHex: 0xBDBDBD)
    }
    
    public class func BDE300() -> UIColor {
        return UIColor(netHex: 0xD3D3D3)
    }
    
    public class func BDE200() -> UIColor {
        return UIColor(netHex: 0xE9E9E9)
    }
    
    public class func BDE100() -> UIColor {
        return UIColor(netHex: 0xF4F4F4)
    }
    
    public class func BDEWhite() -> UIColor {
        return UIColor(netHex: 0xFFFFFF)
    }
    
    public class func BDERed() -> UIColor {
        return UIColor(netHex: 0xda3851)
    }

    public class func BDEWhiteRed() -> UIColor {
        return UIColor(netHex: 0xf4c3ca)
    }

    public class func BDELightRed() -> UIColor {
        return UIColor(netHex: 0xe77d8e)
    }

    public class func BDEDarkRed() -> UIColor {
        return UIColor(netHex: 0xb92a45)
    }
    
    public class func BDEOrange() -> UIColor {
        return UIColor(netHex: 0xf7893b)
    }
    
    public class func BDEWhiteOrange() -> UIColor {
        return UIColor(netHex: 0xfde7d8)
    }
    
    public class func BDELightOrange() -> UIColor {
        return UIColor(netHex: 0xfab27f)
    }
    
    public class func BDEDarkOrange() -> UIColor {
        return UIColor(netHex: 0xd8732c)
    }
    
    public class func BDEYellow() -> UIColor {
        return UIColor(netHex: 0xf8cd51)
    }
    
    public class func BDEWhiteYellow() -> UIColor {
        return UIColor(netHex: 0xfef5dc)
    }
    
    public class func BDELightYellow() -> UIColor {
        return UIColor(netHex: 0xfade8e)
    }
    
    public class func BDEDarkYellow() -> UIColor {
        return UIColor(netHex: 0xc49735)
    }
    
    public class func BDEGold() -> UIColor {
        return UIColor(netHex: 0xd8be75)
    }
    
    public class func BDEWhiteGold() -> UIColor {
        return UIColor(netHex: 0xf3ebd5)
    }
    
    public class func BDELightGold() -> UIColor {
        return UIColor(netHex: 0xe6d5a5)
    }
    
    public class func BDEDarkGold() -> UIColor {
        return UIColor(netHex: 0xb79e5e)
    }

    public class func BDECoral() -> UIColor {
        return UIColor(netHex: 0xf35e61)
    }
    
    public class func BDEWhiteCoral() -> UIColor {
        return UIColor(netHex: 0xfcdfdf)
    }
    
    public class func BDELightCoral() -> UIColor {
        return UIColor(netHex: 0xf59799)
    }
    
    public class func BDEDarkCoral() -> UIColor {
        return UIColor(netHex: 0xd44b50)
    }
    
    public class func BDEGreen() -> UIColor {
        return UIColor(netHex: 0x48ae64)
    }
    
    public class func BDEWhiteGreen() -> UIColor {
        return UIColor(netHex: 0xd9efe0)
    }
    
    public class func BDELightGreen() -> UIColor {
        return UIColor(netHex: 0x88ca9a)
    }
    
    public class func BDEDarkGreen() -> UIColor {
        return UIColor(netHex: 0x388d4f)
    }
    
    public class func BDEPink() -> UIColor {
        return UIColor(netHex: 0xf78be8)
    }
    
    public class func BDEWhitePink() -> UIColor {
        return UIColor(netHex: 0xfddcf8)
    }
    
    public class func BDELightPink() -> UIColor {
        return UIColor(netHex: 0xfab3f0)
    }
    
    public class func BDEDarkPink() -> UIColor {
        return UIColor(netHex: 0xc569b9)
    }
    
    public class func BDEPurple() -> UIColor {
        return UIColor(netHex: 0x8f7ae5)
    }
    
    public class func BDEWhitePurple() -> UIColor {
        return UIColor(netHex: 0xddd7f7)
    }
    
    public class func BDELightPurple() -> UIColor {
        return UIColor(netHex: 0xb6a8ee)
    }
    
    public class func BDEDarkPurple() -> UIColor {
        return UIColor(netHex: 0x7c6ac7)
    }

	public class func BDELightDullGreen() -> UIColor{
		return UIColor(netHex: 0xdaefe0)
	}
	

}
