//
//  BGMExtension-CALayer.swift
//  BGMCoreUISDKIOS
//
//  Created by Pradeep Gangabhathina on 3/17/17.
//  Copyright © 2017 BBVA. All rights reserved.
//

import UIKit
import Foundation

extension CALayer {
    
    func pause() {
        let pauseTime = self.convertTime(CACurrentMediaTime(), from: nil)
        self.speed = 0.0
        self.timeOffset = pauseTime
    }
    
    func resume() {
        let pausedTime = self.timeOffset
        self.speed = 1.0
        self.timeOffset = 0.0
        self.beginTime = 0.0
        let timeSincePause = self.convertTime(CACurrentMediaTime(), to: nil) - pausedTime
        
        self.beginTime = timeSincePause
    }
    
    public func prepareAnimation(keyPath: String, fromValue: Any?, toValue: Any?, beginTime:CFTimeInterval, duration: CFTimeInterval, delegate: CAAnimationDelegate?, key: String?)
    {
        let basicAnimation : CABasicAnimation = CABasicAnimation(keyPath: keyPath)
        
        if (fromValue != nil)
        {
            basicAnimation.fromValue = fromValue
        }
        basicAnimation.toValue = toValue
        basicAnimation.beginTime = beginTime
        basicAnimation.duration = duration
        basicAnimation.fillMode = kCAFillModeForwards
        basicAnimation.isRemovedOnCompletion = false
        basicAnimation.delegate = delegate
        self.add(basicAnimation, forKey: key)
    }
    
    public func prepareAnimation(keyPath: String, fromValue: Any?, toValue: Any?, beginTime:CFTimeInterval, duration: CFTimeInterval, delegate: CAAnimationDelegate?, id: String, isRemovedOnComplete: Bool) -> CAAnimation
    {
        let basicAnimation : CABasicAnimation = CABasicAnimation(keyPath: keyPath)
        
        if (fromValue != nil)
        {
            basicAnimation.fromValue = fromValue
        }
        basicAnimation.toValue = toValue
        basicAnimation.beginTime = beginTime
        basicAnimation.duration = duration
        basicAnimation.fillMode = kCAFillModeForwards
        basicAnimation.isRemovedOnCompletion = isRemovedOnComplete
        basicAnimation.delegate = delegate
        
        basicAnimation.setValue(id, forKey: "animationID")
        return basicAnimation
    }
}
