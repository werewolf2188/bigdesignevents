//
//  BGMExtension-UIButton.swift
//  BGMUICatalog
//
//  Created by Enrique Ricalde on 4/25/17.
//  Copyright © 2017 BBVA. All rights reserved.
//

import UIKit

public extension UIButton {
    
    public func setButtonWithTextBehindImage ()
    {

        let spacing = CGFloat(10.0)
        let imageSize = self.imageView?.frame.size
        self.titleEdgeInsets = UIEdgeInsetsMake(0.0, -(imageSize?.width)!, -((imageSize?.height)! + spacing), 0.0)
        let titleSize = self.titleLabel?.frame.size        
        self.imageEdgeInsets = UIEdgeInsetsMake(-((titleSize?.height)!), self.frame.width/2 - ((imageSize?.width)! / 2) - 2, 0.0, 0.0)

    }
}
