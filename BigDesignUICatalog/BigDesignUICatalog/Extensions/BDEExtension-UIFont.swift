//
//  BGMExtension-UIFont.swift
//  BGMUICatalog
//
//  Created by Enrique Ricalde on 4/25/17.
//  Copyright © 2017 BDE. All rights reserved.
//

import UIKit

public extension UIFont {
    
    static func BDEBlack(size: CGFloat) -> UIFont?
    {
        return UIFont(name: "Lato-Black", size: size)
    }
    
    static func BDEBlackItalic(size: CGFloat) -> UIFont?
    {
        return UIFont(name: "Lato-BlackItalic", size: size)
    }
    
    static func BDEBold(size: CGFloat) -> UIFont?
    {
        return UIFont(name: "Lato-Bold", size: size)
    }
    
    static func BDEBoldItalic(size: CGFloat) -> UIFont?
    {
        return UIFont(name: "Lato-BoldItalic", size: size)
    }
    
    static func BDEHairline(size: CGFloat) -> UIFont?
    {
        return UIFont(name: "Lato-Hairline", size: size)
    }
    
    static func BDEHairlineItalic(size: CGFloat) -> UIFont?
    {
        return UIFont(name: "Lato-HairlineItalic", size: size)
    }
    
    static func BDEItalic(size: CGFloat) -> UIFont?
    {
        return UIFont(name: "Lato-Italic", size: size)
    }
    
    static func BDELight(size: CGFloat) -> UIFont?
    {
        return UIFont(name: "Lato-Light", size: size)
    }
    
    static func BDELightItalic(size: CGFloat) -> UIFont?
    {
        return UIFont(name: "Lato-LightItalic", size: size)
    }
    
    static func BDEMedium(size: CGFloat) -> UIFont?
    {
        return UIFont(name: "Lato-Medium", size: size)
    }
    
    static func BDEMediumItalic(size: CGFloat) -> UIFont?
    {
        return UIFont(name: "Lato-MediumItalic", size: size)
    }
    
    static func BDERegular(size: CGFloat) -> UIFont?
    {
        return UIFont(name: "Lato-Regular", size: size)
    }
    
    static func BDESemiBold(size: CGFloat) -> UIFont?
    {
        return UIFont(name: "Lato-SemiBold", size: size)
    }
    
    static func BDESemiBoldItalic(size: CGFloat) -> UIFont?
    {
        return UIFont(name: "Lato-SemiBoldItalic", size: size)
    }
    
    static func BDEThin(size: CGFloat) -> UIFont?
    {
        return UIFont(name: "Lato-Thin", size: size)
    }
    
    static func BDEThinItalic(size: CGFloat) -> UIFont?
    {
        return UIFont(name: "Lato-ThinItalic", size: size)
    }
    
    static func installFonts()
    {
        let fonts : [String] = ["Lato-Black", "Lato-BlackItalic", "Lato-Bold", "Lato-BoldItalic", "Lato-Hairline", "Lato-HairlineItalic", "Lato-Italic", "Lato-Light", "Lato-LightItalic", "Lato-Medium", "Lato-MediumItalic", "Lato-Regular" , "Lato-Semibold", "Lato-SemiboldItalic", "Lato-Thin", "Lato-ThinItalic"]

        let frameworkBundle : Bundle = Bundle(for: BDESnackbar.self)
        for font in fonts
        {
            
            let pathForResourceString = frameworkBundle.path(forResource: font, ofType: "ttf")
            
            let fontData = NSData(contentsOfFile: pathForResourceString!)
            let dataProvider = CGDataProvider(data: fontData!)
            let fontRef = CGFont(dataProvider!)
            var errorRef: Unmanaged<CFError>? = nil
            
            if (CTFontManagerRegisterGraphicsFont(fontRef, &errorRef) == false) {
                NSLog("Failed to register font - register graphics font failed - this font may have already been registered in the main bundle.")
            }
        }
    }
}
