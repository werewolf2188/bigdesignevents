//
//  BGMExtension-FloatingPoint.swift
//  BGMCoreUISDKIOS
//
//  Created by Pradeep Gangabhathina on 3/17/17.
//  Copyright © 2017 BBVA. All rights reserved.
//

import UIKit
import Foundation

public extension FloatingPoint {
    
    var degreesToRadians: Self { return self * .pi / 180 }
    var radiansToDegrees: Self { return self * 180 / .pi }
    
}
