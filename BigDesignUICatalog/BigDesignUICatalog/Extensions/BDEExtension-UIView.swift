//
//  BDEExtension-UIView.swift
//  BDECoreUISDKIOS
//
//  Created by Pradeep Gangabhathina on 3/17/17.
//  Copyright © 2017 BBVA. All rights reserved.
//

import UIKit
import Foundation

public extension UIView
{
    public func copyView() -> AnyObject
    {
        return NSKeyedUnarchiver.unarchiveObject(with: NSKeyedArchiver.archivedData(withRootObject: self))! as AnyObject
    }
    
    func addAdjacentConstraints(view:UIView, superView:UIView, offset:CGFloat) {
        let leftLeadingConstraint = NSLayoutConstraint(item: view, attribute: NSLayoutAttribute.leading, relatedBy: NSLayoutRelation.equal
            , toItem: superView, attribute: NSLayoutAttribute.leading, multiplier: 1, constant: offset)
        
        let topConstraint = NSLayoutConstraint(item: view, attribute: NSLayoutAttribute.top, relatedBy: NSLayoutRelation.equal
            , toItem: superView, attribute: NSLayoutAttribute.top, multiplier: 1, constant: offset)
        
        let bottomConstraint = NSLayoutConstraint(item: view, attribute: NSLayoutAttribute.bottom, relatedBy: NSLayoutRelation.equal
            , toItem: superView, attribute: NSLayoutAttribute.bottom, multiplier: 1, constant: offset)
        
        let rightConstraint = NSLayoutConstraint(item: view, attribute: NSLayoutAttribute.trailing, relatedBy: NSLayoutRelation.equal
            , toItem: superView, attribute: NSLayoutAttribute.trailing, multiplier: 1, constant: offset)
        
        NSLayoutConstraint.activate([topConstraint, leftLeadingConstraint, bottomConstraint, rightConstraint])
    }
    
    public static func addDiagonalBottomCut(currentView:UIView){
        let maskLayer = CAShapeLayer()
        maskLayer.frame = currentView.bounds
        
        let width  = currentView.frame.size.width
        let height = currentView.frame.size.height
        let path = CGMutablePath()
        let tiltAngle : CGFloat = 80
        let diagonal : CGFloat = (currentView.frame.height / tan(tiltAngle.degreesToRadians))
        //CGPoint(x: width + width/4, y: height - (height/3))
        path.move(to: CGPoint(x: 0, y: 0))
        path.addLine(to: CGPoint(x: 0, y: height))
        path.addLine(to: CGPoint(x: width * 2, y: height - diagonal))
        path.addLine(to: CGPoint(x: width * 2, y: 0))
        path.addLine(to: CGPoint(x: 0, y: 0))
        
        maskLayer.path = path
        currentView.layer.mask = maskLayer
    }
    
    func nativeScale() -> CGFloat{
        let mainScreen: UIScreen = UIScreen.main
        let scale : CGFloat = mainScreen.scale
        if(mainScreen.bounds.size.height * scale == 1136){return 1.0}
        return mainScreen.nativeScale
    }
    
    public func showSnackbarMessage(message: String, duration: BDESnackbarDuration, actionText: String, actionBlock: BDESnackbar.BDEActionBlock? = nil)
    {
        BDESnackbar.sharedSnackbar.message = message
        BDESnackbar.sharedSnackbar.duration = duration
        BDESnackbar.sharedSnackbar.actionText = actionText
        BDESnackbar.sharedSnackbar.actionBlock = actionBlock
        BDESnackbar.sharedSnackbar.leftMargin = 0
        BDESnackbar.sharedSnackbar.rightMargin = 0
        BDESnackbar.sharedSnackbar.bottomMargin = 0
        BDESnackbar.sharedSnackbar.cornerRadius = 0
        BDESnackbar.sharedSnackbar.messageTextAlign = .center
        BDESnackbar.sharedSnackbar.height = 100
        BDESnackbar.sharedSnackbar.messageTextFont = UIFont.BDEMedium(size: 14)!
        BDESnackbar.sharedSnackbar.backgroundColor = UIColor.BDEGreen()
        BDESnackbar.sharedSnackbar.contentInset = UIEdgeInsets(top: 30, left: 30, bottom: 30, right: 30)
        if (!BDESnackbar.sharedSnackbar.isShowing)
        {
            BDESnackbar.sharedSnackbar.show()
        }
    }
    
    public func showSnackbarError(message: String, duration: BDESnackbarDuration, actionText: String, actionBlock: BDESnackbar.BDEActionBlock? = nil)
    {
        BDESnackbar.sharedSnackbar.message = message
        BDESnackbar.sharedSnackbar.duration = duration
        BDESnackbar.sharedSnackbar.actionText = actionText
        BDESnackbar.sharedSnackbar.actionBlock = actionBlock
        BDESnackbar.sharedSnackbar.leftMargin = 0
        BDESnackbar.sharedSnackbar.rightMargin = 0
        BDESnackbar.sharedSnackbar.bottomMargin = 0
        BDESnackbar.sharedSnackbar.cornerRadius = 0
        BDESnackbar.sharedSnackbar.messageTextAlign = .center
        BDESnackbar.sharedSnackbar.height = 100
        BDESnackbar.sharedSnackbar.messageTextFont = UIFont.BDEMedium(size: 14)!
        BDESnackbar.sharedSnackbar.backgroundColor = UIColor.BDERed()
        BDESnackbar.sharedSnackbar.contentInset = UIEdgeInsets(top: 30, left: 30, bottom: 30, right: 30)
        if (!BDESnackbar.sharedSnackbar.isShowing)
        {
            BDESnackbar.sharedSnackbar.show()
        }
    }
    
    public func showComingSoon()
    {
        BDESnackbar.sharedSnackbar.message = "Coming Soon"
        BDESnackbar.sharedSnackbar.duration = .long
        BDESnackbar.sharedSnackbar.actionText = ""
        BDESnackbar.sharedSnackbar.actionBlock = nil
        BDESnackbar.sharedSnackbar.leftMargin = 0
        BDESnackbar.sharedSnackbar.rightMargin = 0
        BDESnackbar.sharedSnackbar.bottomMargin = 0
        BDESnackbar.sharedSnackbar.cornerRadius = 0
        BDESnackbar.sharedSnackbar.messageTextAlign = .center
        BDESnackbar.sharedSnackbar.height = 100
        BDESnackbar.sharedSnackbar.messageTextFont = UIFont.BDEMedium(size: 14)!
        BDESnackbar.sharedSnackbar.backgroundColor = UIColor.BDERed()
        BDESnackbar.sharedSnackbar.contentInset = UIEdgeInsets(top: 30, left: 30, bottom: 30, right: 30)
        if (!BDESnackbar.sharedSnackbar.isShowing)
        {
            BDESnackbar.sharedSnackbar.show()
        }
        
    }

    
    public func traverseResponderChainForUIViewController() -> UIViewController?
    {
        let responder : UIResponder? = self.next
        
        if (responder != nil)
        {
            if (responder! is UIViewController)
            {
                return responder as? UIViewController
            }
            else if (responder! is UIView)
            {
                return (responder! as! UIView).traverseResponderChainForUIViewController()
            }
            else
            {
                return nil
            }
        }
        else
        {
            return nil
        }
    }
}
