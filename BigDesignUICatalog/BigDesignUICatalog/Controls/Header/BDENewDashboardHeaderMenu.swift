//
//  BGMNewDashboardHeaderMenu.swift
//  BGMAccountsUIIos
//
//  Created by Enrique Ricalde on 8/7/17.
//  Copyright © 2017 BBVA. All rights reserved.
//

import UIKit
import BigDesignManager
public class BDENewDashboardHeaderMenu: UIView, CAAnimationDelegate {

    
    let circularLayerName : String = "circularLayer"
    let correctnesForFourCorner : CGFloat = -0.2
    let correctnesForFourSize : CGFloat = -0.33
    @IBOutlet weak var imageView : UIImageView!
    @IBOutlet weak var firstButton : BDEButton!
    @IBOutlet weak var secondButton : BDEButton!
    @IBOutlet weak var thirdButton : BDEButton!
    @IBOutlet weak var fourthButton : BDEButton!
    @IBOutlet weak var firstTitle : UILabel!
    @IBOutlet weak var secondTitle : UILabel!
    @IBOutlet weak var thirdTitle : UILabel!
    @IBOutlet weak var fourthTitle : UILabel!
    @IBOutlet weak var titleLabel : UILabel!
    @IBOutlet weak var initialLabel : UILabel!
    @IBOutlet weak var welcomeContainer : UIView!
    @IBOutlet public weak var operationsContainer: UIView!
    
    @IBOutlet weak var nameTopConstraint: NSLayoutConstraint!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    required public init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    var loaded : Bool = false
    
    private var _dashboard : UIViewController!
    public var dashboard : UIViewController!
    {
        get {
            return _dashboard
        }
        set
        {
            _dashboard = newValue
        }
    }
    
    public var operations: [Action]?
    {
        didSet
        {
            loadDataToMenuHeader()
        }
    }
    
    override public func layoutSubviews() {
        super.layoutSubviews()
        
        self.backgroundColor = UIColor.clear
        setHeaderButtonBackgroundColor(button: firstButton)
        setHeaderButtonBackgroundColor(button: secondButton)
        setHeaderButtonBackgroundColor(button: thirdButton)
        setHeaderButtonBackgroundColor(button: fourthButton)
        initialLabel.textColor = UIColor.white
    }
    
    public func setHeaderButtonBackgroundColor(button: UIButton)
    {
        let lightAmount : CGFloat = 0.10
        button.backgroundColor = UIColor.white.withAlphaComponent(lightAmount)
        
    }
    
    
    public func showNextAction(gesture : UITapGestureRecognizer)
    {
        
        if (!(self.viewWithTag(gesture.view!.tag - 4) as! BDEButton).showComingSoonAlert)
        {
            (self.viewWithTag(gesture.view!.tag - 4) as! BDEButton).OnDoneAnimation((self.viewWithTag(gesture.view!.tag - 4) as! BDEButton))
        }
        else {
            gesture.view!.showComingSoon()
        }
    }
    
    public func returnOperativeActionImage(id: String) -> UIImage?
    {
        return UIImage()
    }
    
    public func hasOperativeAction(id: String) -> Bool
    {
        return false
    }
    
    public func setUserInfo(user: User?, animate: Bool = true)
    {
        let firstName : String = user != nil ? user!.firstName! : ""
        let lastName : String? =  user != nil ?  user!.lastName : nil
        
        imageView.image = nil
        imageView.backgroundColor = UIColor.white
        imageView.layer.cornerRadius = imageView.frame.width / 2
        var initials : String = firstName.characters.count > 0 ? firstName.substring(to: firstName.index(firstName.startIndex, offsetBy: 1)) : ""
        
        if (lastName != nil)
        {
            initials += lastName!.substring(to: lastName!.index(lastName!.startIndex, offsetBy: 1))
        }
        else
        {
            initials += firstName.characters.count > 0 ? firstName.substring(to: firstName.index(firstName.startIndex, offsetBy: 2)) : ""
        }
        initialLabel.text = "\(initials.uppercased())"
        
        
        self.titleLabel.superview!.clipsToBounds = true
        titleLabel.text = "Hello \(firstName)"
        
        initialLabel.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.navigateToUserProfile)))
        titleLabel.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.navigateToUserProfile)))
        imageView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.navigateToUserProfile)))
        
        firstTitle.text = ""
        secondTitle.text = ""
        thirdTitle.text = ""
        fourthTitle.text = ""
        
        initializeViewsTransformRadius(animate: animate)       
        
    }
    
    public func scrollableOpacity(_ offset : CGPoint)
    {
        var limit : CGFloat = 1
        if (operations != nil && operations!.count > 0)
        {
            limit = BDETableViewHeights.DAHSBOARD_HEADER
        }
        else
        {
            limit = BDETableViewHeights.DAHSBOARD_HEADER_NOT_OPERATIONS
        }
        if (offset.y < limit)
        {
            let opacity : CGFloat = offset.y / limit
            imageView.alpha = 1 - opacity
            initialLabel.alpha = 1 - opacity
            titleLabel.alpha = 1 - opacity
            
            firstButton.alpha = 1 - opacity
            firstTitle.alpha = 1 - opacity
            secondButton.alpha = 1 - opacity
            secondTitle.alpha = 1 - opacity
            thirdButton.alpha = 1 - opacity
            thirdTitle.alpha = 1 - opacity
            fourthButton.alpha = 1 - opacity
            fourthTitle.alpha = 1 - opacity
        }
        
    }
    
    
    public func radius(_ width : CGFloat) -> CGFloat
    {
        let halfWidth : CGFloat = (width ) / 2
        var roundedScale : CGFloat = (ceil(UIScreen.main.scale) / 2) > 1 ? (ceil(UIScreen.main.scale) / 2) / 10 : 0
        if UIDevice.current.deviceSize == .Four
        {
            roundedScale = correctnesForFourCorner
        }
        return halfWidth * (1 + roundedScale)
    }
    
    public func initializeViewsTransformRadius(animate : Bool = true)
    {
        let setupTransform : ((_ vie : UIView) -> Void) = { (vie) in
            vie.layer.anchorPoint = CGPoint(x: 0.5, y: 0.5)
            vie.layer.transform = CATransform3DScale(vie.layer.transform, 0, 0, 0)

        }
        
        let setupRadius : ((_ vie : UIView) -> Void) = { (vie) in
            //let width : CGFloat = self.firstButton.constraints.filter({$0.firstAttribute == .width }).first!.constant
            vie.layoutIfNeeded()
            vie.layer.cornerRadius = self.radius(vie.frame.width )
            vie.layer.rasterizationScale = UIScreen.main.scale
            vie.layer.addSublayer(self.addCircularLayer ())
            
        }
        
        
        //This for grouping
        let tops : [NSLayoutConstraint] =  returnTopConstraints()
        let setupTitle : ((_ vie : UIView) -> Void) = { (vie) in
            let top : CGFloat = tops.filter({($0.firstItem as! UIView) == vie}).first!.constant
            
            tops.filter({($0.firstItem as! UIView) == vie}).first!.constant = top + vie.frame.height
        }
        
        setupRadius(firstButton)
        setupRadius(secondButton)
        setupRadius(thirdButton)
        setupRadius(fourthButton)
        
        if (animate)
        {
            setupTransform(imageView)
            setupTransform(initialLabel)
            setupTransform(firstButton)
            setupTransform(secondButton)
            setupTransform(thirdButton)
            setupTransform(fourthButton)
            
            setupTitle(firstTitle)
            setupTitle(secondTitle)
            setupTitle(thirdTitle)
            setupTitle(fourthTitle)
        }
        
        
        nameTopConstraint.constant = self.titleLabel.frame.size.height * -1
    }
    
    public func addCircularLayer () -> CALayer
    {
        var roundedScale : CGFloat = (ceil(UIScreen.main.scale) / 2) / 10
        if UIDevice.current.deviceSize == .Four
        {
            roundedScale = correctnesForFourSize
        }
        roundedScale = (ceil(UIScreen.main.scale) / 2) > 1 || UIDevice.current.deviceSize == .Four ? 1 + roundedScale : 1
        let subLayer : CALayer = CALayer()
        let size : CGFloat = 22.2
        subLayer.frame = CGRect(x: size * roundedScale, y: size * roundedScale, width: size, height: size)
        subLayer.opacity = 0.4
        subLayer.backgroundColor = UIColor.white.cgColor
        subLayer.cornerRadius = subLayer.frame.width / 2
        subLayer.name = circularLayerName
        return subLayer
    }
    
    public func animateButtonTransform()
    {
        let setupTransform : ((_ vie : UIView) -> Void) = { (vie) in
            vie.layer.prepareAnimation(keyPath: "transform.scale", fromValue: NSNumber(value: 0), toValue: NSNumber(value: 1), beginTime: CACurrentMediaTime(), duration: CATransaction.animationDuration() * 2, delegate: self, key: "")
            
        }
        
        DispatchQueue.main.async {

            self.nameTopConstraint.constant = 0
            
            UIView.animate(withDuration: CATransaction.animationDuration() * 2, animations: { 
                self.titleLabel.superview!.layoutIfNeeded()
            })
            setupTransform(self.imageView)
            setupTransform(self.initialLabel)
            setupTransform(self.firstButton)
            setupTransform(self.secondButton)
            setupTransform(self.thirdButton)
            setupTransform(self.fourthButton)
            
        }
        
    }
    
    public func animationDidStop(_ anim: CAAnimation, finished flag: Bool)
    {
        var bgmButtons : [BDEButton] = [firstButton, secondButton, thirdButton, fourthButton]
        if (anim.value(forKey: "animationID") == nil)
        {
            imageView.layer.transform = CATransform3DIdentity
            initialLabel.layer.transform = CATransform3DIdentity
            firstButton.layer.transform = CATransform3DIdentity
            secondButton.layer.transform = CATransform3DIdentity
            thirdButton.layer.transform = CATransform3DIdentity
            fourthButton.layer.transform = CATransform3DIdentity
            
            addPulsatingAnimation(firstButton, 0, true, false)
            addPulsatingAnimation(secondButton, 1, true, false)
            addPulsatingAnimation(thirdButton, 2, true, false)
            addPulsatingAnimation(fourthButton, 3, true, false)
        }
        else if((anim.value(forKey: "animationID") as! String).contains("pulse") && !loaded) {
            let id : String = (anim.value(forKey: "animationID") as! String).replacingOccurrences(of: "pulse", with: "")
            let idNumber : Int? = Int(id)
            
            if (idNumber != nil)
            {
                addPulsatingAnimation(bgmButtons[idNumber!], CFTimeInterval(idNumber!), false, (anim.value(forKey: "reverse") as! Bool))
            }
        }

    }
    
    public func addPulsatingAnimation(_ button: BDEButton, _ extraTime : CFTimeInterval, _ start : Bool, _ reverse : Bool)
    {
        let basicAnimation : CABasicAnimation = CABasicAnimation(keyPath: "opacity")
        let durationAnim : CFTimeInterval = CATransaction.animationDuration() * 4
        basicAnimation.fromValue = reverse ? 0.1 : 0.4
        basicAnimation.toValue = reverse ? 0.4 : 0.1
        
        basicAnimation.beginTime = start ? CACurrentMediaTime() + (extraTime / 4) : CACurrentMediaTime()
        basicAnimation.duration = durationAnim
        basicAnimation.fillMode = kCAFillModeForwards
        basicAnimation.isRemovedOnCompletion = false
        basicAnimation.delegate = self
        basicAnimation.repeatCount = 1
        //basicAnimation.autoreverses = true
        basicAnimation.setValue("pulse\(Int(extraTime))", forKey: "animationID")
        basicAnimation.setValue(!reverse, forKey: "reverse")
        
        button.layer.sublayers?.filter({$0.name == circularLayerName }).first?.add(basicAnimation, forKey: nil)
        
    }
    
    public func navigateToUserProfile() {
        self.showComingSoon()
        
    }
    
    public func setOperation(title: UILabel, button: BDEButton, operation: Action)
    {
        if (!title.isHidden)
        {
            title.text = operation.name
           
            button.layer.removeAllAnimations()
            button.layer.sublayers?.filter({$0.name == circularLayerName }).first?.removeFromSuperlayer()
            button.setImage(returnOperativeActionImage(id: operation.id!), for: .normal)
            button.showComingSoonAlert = !self.hasOperativeAction(id: operation.id!)
            button.accessibilityIdentifier = (operation.id?.replacingOccurrences(of: "_", with: " ").capitalized.replacingOccurrences(of: " ", with: ""))! + "Btn"
            button.superview!.accessibilityIdentifier = (operation.id?.replacingOccurrences(of: "_", with: " ").capitalized.replacingOccurrences(of: " ", with: ""))! + "Layout"
            title.accessibilityIdentifier = (operation.id?.replacingOccurrences(of: "_", with: " ").capitalized.replacingOccurrences(of: " ", with: ""))! + "Lnk"
            button.OnDoneAnimation = { (button) in
                if (!button.showComingSoonAlert)
                {
                    self.operativeAction(id: operation.id!)
                }
            }
            //Need to consider if they're not more than 2 lines
            if (title.text!.components(separatedBy: CharacterSet(charactersIn:" ")).count == 1)
            {
                title.numberOfLines = 1
            }
        }
    }
    
    public func returnTopConstraints() -> [NSLayoutConstraint]
    {
        var consTop :  [NSLayoutConstraint] = []
        for subview in self.operationsContainer.subviews
        {
            consTop.append(contentsOf: subview.constraints.filter({$0.firstAttribute == .top}))
        }
        
        return consTop
    }
    
    public func loadDataToMenuHeader()
    {
        var fourOptions : [Action] = self.operations!
        fourOptions = fourOptions.count >= 4 ? Array(fourOptions[0...3]) : fourOptions
        
        //Hide buttons
        fourthTitle.isHidden = fourOptions.count < 4
        fourthButton.isHidden = fourOptions.count < 4
        thirdTitle.isHidden = fourOptions.count < 3
        thirdButton.isHidden = fourOptions.count < 3
        secondTitle.isHidden = fourOptions.count < 2
        secondButton.isHidden = fourOptions.count < 2
        firstTitle.isHidden = fourOptions.count < 1
        firstButton.isHidden = fourOptions.count < 1
        
        //This for grouping
        let tops : [NSLayoutConstraint] =  returnTopConstraints()
        let realTop : CGFloat = 5
        let setupTitle : ((_ vie : UIView) -> Void) = { (vie) in
            
            tops.filter({($0.firstItem as! UIView) == vie}).first!.constant = realTop
        }
        

        //These ifs are in case there's less than 4 options
        if (!firstTitle.isHidden)
        {
            setOperation(title: firstTitle, button: firstButton, operation: fourOptions[0])
            setupTitle(firstTitle)
        }
        else {
            firstButton.layer.removeAllAnimations()
            firstButton.layer.sublayers?.filter({$0.name == circularLayerName }).first?.removeFromSuperlayer()
        }
        if (!secondTitle.isHidden)
        {
            setOperation(title: secondTitle, button: secondButton, operation: fourOptions[1])
            setupTitle(secondTitle)
        }
        else {
            secondButton.layer.removeAllAnimations()
            secondButton.layer.sublayers?.filter({$0.name == circularLayerName }).first?.removeFromSuperlayer()
        }
        if (!thirdTitle.isHidden)
        {
            setOperation(title: thirdTitle, button: thirdButton, operation: fourOptions[2])
            setupTitle(thirdTitle)
        }
        else {
            thirdButton.layer.removeAllAnimations()
            thirdButton.layer.sublayers?.filter({$0.name == circularLayerName }).first?.removeFromSuperlayer()
        }
        if (!fourthTitle.isHidden)
        {
            setOperation(title: fourthTitle, button: fourthButton, operation: fourOptions[3])
            setupTitle(fourthTitle)
        }
        else {
            fourthButton.layer.removeAllAnimations()
            fourthButton.layer.sublayers?.filter({$0.name == circularLayerName }).first?.removeFromSuperlayer()
        }
        loaded = true
    }

    //Need to refactor this
    public func operativeAction(id : String)
    {
        self.showComingSoon()
    }
    
    @IBAction func onFeedback(_ sender: Any)
    {
        self.showComingSoon()
    }

    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */

}
