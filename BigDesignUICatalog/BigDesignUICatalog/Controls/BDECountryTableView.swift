//
//  BDECountryTableView.swift
//  BigDesignUICatalog
//
//  Created by Enrique on 9/9/17.
//  Copyright © 2017 werewolf. All rights reserved.
//

import Foundation
import BigDesignManager
public class BDEContryTableView: BDETableView {
    
    public var continent : Continent!
    /*
     // Only override draw() if you perform custom drawing.
     // An empty implementation adversely affects performance during animation.
     override func draw(_ rect: CGRect) {
     // Drawing code
     }
     */
    
    override public func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        
        return BDETableViewHeights.header
    }
    
    override public func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return getHeightForItem(object: self.products[indexPath.row])
    }
    
    override public func getHeightForItem(object: AnyObject) -> CGFloat
    {
        return BDETableViewHeights.country
    }
    
    override public func tableView(_ tableView: UITableView, willDisplayHeaderView view: UIView, forSection section: Int) {
        let cardView : BDEHeaderCardView = (Bundle(for: BDEHeaderCardView.self).loadNibNamed("BDEHeaderCardView", owner: nil, options: nil)?.first as? BDEHeaderCardView)!
        cardView.titleLabel.text = continent.name
        var frame : CGRect = view.frame
        frame.origin = CGPoint(x: 0, y: 0)
        cardView.frame = frame
        view.addSubview(cardView)
    }
    
    override public func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        var cardView : BDECountryCardView
        cardView = (Bundle(for: BDECountryCardView.self).loadNibNamed("BDECountryCardView", owner: nil, options: nil)?.first as? BDECountryCardView)!
        cardView.populateCellFor(country: self.products[indexPath.row] as! Country)
        var frame : CGRect = cell.frame
        frame.origin = CGPoint(x: 0, y: 0)
        cardView.frame = frame
        cardView.row = TimeInterval(self.products.count) - (TimeInterval(indexPath.row))
        cardView.tag = 3
        cell.contentView.addSubview(cardView)
    }
}
