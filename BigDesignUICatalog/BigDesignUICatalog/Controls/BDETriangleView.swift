//
//  BDETriangleView.swift
//  BigDesignUICatalog
//
//  Created by Enrique on 9/12/17.
//  Copyright © 2017 werewolf. All rights reserved.
//

import Foundation

public class BDETriangleView : UIView
{
    public var animate : Bool = true
    required public init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
    }
    
    override public init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    override public func draw(_ rect: CGRect) {
        addDiagonalBottomCut(rect)
    }
    
    func addDiagonalBottomCut(_ rect: CGRect){
        let maskLayer = CAShapeLayer()
        
        let width : CGFloat = UIScreen.main.bounds.width
        let height : CGFloat = self.layer.frame.height
        let percentageSustracted: CGFloat = 20
        
        let sustractedHeight : CGFloat = (percentageSustracted * height) / 100
        
        let path = CGMutablePath()
        let tiltAngle : CGFloat = 50
        let diagonal : CGFloat = (height / tan(tiltAngle.degreesToRadians))
        //CGPoint(x: width + width/4, y: height - (height/3))
        path.move(to: CGPoint(x: 0, y: 0))
        path.addLine(to: CGPoint(x: 0, y: height - sustractedHeight))
        path.addLine(to: CGPoint(x: width * 2, y: height - diagonal - sustractedHeight))
        path.addLine(to: CGPoint(x: width * 2, y: 0))
        path.addLine(to: CGPoint(x: 0, y: 0))
        
        maskLayer.path = path
        maskLayer.fillColor = UIColor.BDEMorningBlue().cgColor
        
        if (self.layer.mask == nil)
        {
            self.layer.mask = maskLayer
        }
        else
        {
            if (self.animate)
            {
                self.layer.mask!.prepareAnimation(keyPath: "path", fromValue: nil, toValue: path, beginTime: CACurrentMediaTime(), duration: CATransaction.animationDuration(), delegate: nil, key: nil)
            }
            else
            {
                (self.layer.mask as! CAShapeLayer).path = path
            }
        }
    }
    
}
