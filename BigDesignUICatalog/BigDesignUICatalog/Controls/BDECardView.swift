//
//  BGMCardView.swift
//  BGMCoreUISDKIOS
//
//  Created by Enrique Ricalde on 2/3/17.
//  Copyright © 2017 BBVA. All rights reserved.
//

import UIKit

@IBDesignable
open class BDECardView: UIView, CAAnimationDelegate {
    let duration : TimeInterval = CATransaction.animationDuration() * 3//1.25
    
    
    var animationMove : CAAnimation!
    var animationOpacity : CAAnimation!
    var animationHide : CAAnimation!
    var animationGroup : CAAnimationGroup!
    var layer1 : CALayer!
    var layer2 : CAShapeLayer!
    var layer3 : CAShapeLayer!
    
    
    
    public static var isDone : Bool = false
    //public var totalRows : TimeInterval = 0
    
    public var cycles : Int = 0
    public static let kcBGMCardViewDoneNotification : String = "kcBGMCardViewDoneNotification"
    
    public var row : Double = -1
        {
        didSet{
            
            //print(row)
            prepareShadow()
        }
    }
    
    @IBInspectable public var layer1Color : UIColor! = UIColor(netHex: 0xe9e9e9)
    @IBInspectable public var layer2Color : UIColor! = UIColor(netHex: 0xf4f4f4)
    
    override open func layoutSubviews() {
        
        //Need to work on this one
        super.layoutSubviews()
        
        if (!BDECardView.isDone && layer1 != nil)
        {
            prepareLayer3()
            layer1.frame = self.bounds
            //layer2.transform = CATransform3DTranslate(CATransform3DIdentity, self.bounds.width , 0, 0)
            let diagonal : CGFloat = (self.bounds.width * 36) / 100
            layer2.path = createRectForLoader(rect:self.layer.frame)            
            layer2.position = CGPoint(x: layer2.position.x + diagonal, y: layer2.position.y)
        }
    }
    
//    override open func prepareForInterfaceBuilder() {
//        super.prepareForInterfaceBuilder()
//        //prepareShadow()
//    }
    
    required public init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
    }
    
    override public init(frame: CGRect) {
        super.init(frame: frame)
    }

    public func setRectangularCorners(frame:CGRect, corners: UIRectCorner, radius:CGFloat)
    {
        //POC corners
        if (corners.contains(.allCorners))
        {
            upperLeftRectangularCorner(frame: frame, radius: radius)
            upperRightRectangularCorner(frame: frame, radius: radius)
            bottomLeftRectangularCorner(frame: frame, radius: radius)
            bottomRightRectangularCorner(frame: frame, radius: radius)
        }
        else
        {
            if (corners.contains(.topLeft))
            {
                upperLeftRectangularCorner(frame: frame, radius: radius)
            }
            if (corners.contains(.topRight))
            {
                upperRightRectangularCorner(frame: frame, radius: radius)
            }
            if (corners.contains(.bottomLeft))
            {
                bottomLeftRectangularCorner(frame: frame, radius: radius)
            }
            if (corners.contains(.bottomRight))
            {
                bottomRightRectangularCorner(frame: frame, radius: radius)
            }
        }
    }
    
    private func upperLeftRectangularCorner(frame:CGRect, radius:CGFloat)
    {
        let urCorner : UIView = UIView(frame: CGRect(x: 0, y: 0, width: radius, height: radius))
        urCorner.backgroundColor = self.backgroundColor
        urCorner.layer.cornerRadius = 1
        self.addSubview(urCorner)
    }
    
    private func upperRightRectangularCorner(frame:CGRect, radius:CGFloat)
    {
        let urCorner : UIView = UIView(frame: CGRect(x: frame.width - radius, y: 0 , width: radius, height: radius))
        urCorner.backgroundColor = self.backgroundColor
        urCorner.layer.cornerRadius = 1
        self.addSubview(urCorner)
    }
    
    private func bottomLeftRectangularCorner(frame:CGRect, radius:CGFloat)
    {
        let urCorner : UIView = UIView(frame: CGRect(x: 0, y: frame.height - radius, width: radius, height: radius))
        urCorner.backgroundColor = self.backgroundColor
        urCorner.layer.cornerRadius = 1
        self.addSubview(urCorner)
    }
    
    private func bottomRightRectangularCorner(frame:CGRect, radius:CGFloat)
    {
        let urCorner : UIView = UIView(frame: CGRect(x: frame.width - radius, y: frame.height - radius , width: radius, height: radius))
        urCorner.backgroundColor = self.backgroundColor
        urCorner.layer.cornerRadius = 1
        self.addSubview(urCorner)
    }
    
    open func setShadow()
    {
//        self.alpha = 1
//        self.layer.cornerRadius = 2
//        
//        self.layer.shadowOffset = CGSize(width: 0, height: -1)
//        self.layer.shadowRadius = 3
//        //self.layer.shadowPath = UIBezierPath(rect: self.bounds).cgPath
//        self.layer.shadowOpacity = 0.1
//        self.layer.shadowColor = UIColor.BBVA600().cgColor
//        self.layer.allowsEdgeAntialiasing = true
    }
    
    open func removeShadow()
    {
        self.layer.shadowRadius = 0
        self.layer.shadowOffset = CGSize(width: 0, height: 0)
        self.layer.shadowOpacity = 0
    }
    
    func prepareShadow()
    {
        setShadow()
        prepareAnimationLayer()
    }
    
    func prepareAnimationLayer()
    {
        // three layers
        // 1.- background
        // 2.- rolling background that moves in the middle
        // 3.- Clear rects layer
        
        //1
        let diagonal : CGFloat = (self.bounds.width * 36) / 100
        if (!BDECardView.isDone)
        {
            if (self.layer1 == nil) //This layer needs to handle resizing
            {
                layer1 = CALayer()
                layer1.frame = self.bounds
                layer1.masksToBounds = true
                layer1.backgroundColor = self.layer1Color.cgColor
                self.layer.addSublayer(layer1)
            }
            
            if (self.layer2 == nil) //This layer needs to handle resizing
            {
                //2
                layer2 = CAShapeLayer()
                layer2.path = createRectForLoader(rect:self.bounds)
                //layer2.frame = self.bounds
                
                layer2.position = CGPoint(x: layer2.position.x + diagonal, y: layer2.position.y)
                layer2.fillColor = self.layer2Color.cgColor
                layer2.opacity = 1
                layer2.transform = CATransform3DTranslate(CATransform3DIdentity, self.bounds.width, 0, 0)
                self.layer1.addSublayer(layer2)
            }
            
            if (self.layer3 == nil) //This layer needs to handle resizing
            {
                layer3 = CAShapeLayer()
                prepareLayer3()
                self.layer.addSublayer(layer3)
            }
            
            //layer2.mask = layer1
            let beginTime1 : TimeInterval = TimeInterval(TimeInterval(0)) //+ TimeInterval(self.row / self.totalRows - 1))
            let beginTime2 : TimeInterval = beginTime1 + self.duration
            //3
            self.animationMove = layer2.prepareAnimation(keyPath: "position", fromValue: layer2.position, toValue: CGPoint(x: -layer2.position.x - diagonal * 2, y: layer2.position.y), beginTime: beginTime1, duration: self.duration, delegate: self, id: "move", isRemovedOnComplete: false)
            
            self.animationMove.autoreverses = true
            self.animationMove.timingFunction = CAMediaTimingFunction(name:kCAMediaTimingFunctionEaseInEaseOut)
            
            self.animationOpacity = layer2.prepareAnimation(keyPath: "opacity", fromValue: 1, toValue: 0, beginTime: beginTime2, duration: self.duration / 4, delegate: self, id: "opacity", isRemovedOnComplete: false)
            
            //Animations
            self.animationGroup = CAAnimationGroup()
            self.animationGroup.duration = beginTime2 + self.duration / 4
            self.animationGroup.animations = [self.animationMove, self.animationOpacity]
            self.animationGroup.delegate = self
            self.animationGroup.isRemovedOnCompletion = false
            self.animationGroup.setValue("group", forKey: "animationID")
            
            self.animationHide = layer2.prepareAnimation(keyPath: "opacity", fromValue: 1, toValue: 0, beginTime: 0, duration: self.duration, delegate: self, id: "hide", isRemovedOnComplete: false)
            
            layer2.add(self.animationMove, forKey: nil)
        }
        
    }
    
    func prepareLayer3()
    {
        let path : UIBezierPath =  UIBezierPath(rect: self.bounds)
        for count in 0..<self.subviews.count {
            let subView : UIView = self.subviews[count]
            if (subView is UILabel)
            {
                let bezierPath : UIBezierPath  = UIBezierPath(cgPath: createCGPathForLabels(subView.frame, alignment: (subView as? UILabel)!.textAlignment))
                path.append(bezierPath)
            }
            else if(!subView.isHidden)
            {
                let bezierPath : UIBezierPath  = UIBezierPath(roundedRect: subView.frame, cornerRadius: 5)
                path.append(bezierPath)
            }
        }
        
        path.usesEvenOddFillRule = true
        
        
        layer3.path = path.cgPath
        layer3.fillRule = kCAFillRuleEvenOdd
        layer3.fillColor = self.backgroundColor?.cgColor
        layer3.opacity = 1
        layer3.frame = self.bounds
        layer3.masksToBounds = true

    }
    
    public func animationDidStop(_ anim: CAAnimation, finished flag: Bool) {
        //print(flag)
        if ((anim.value(forKey: "animationID")! as! String) == "move" && !BDECardView.isDone)
        {
            layer2.add(self.animationMove, forKey: nil)
        }
        else
        {
            self.layer3.removeFromSuperlayer()
            self.layer2.removeFromSuperlayer()
            self.layer1.removeFromSuperlayer()
        }
        
    }
    
    public func removeLayers()
    {
        if (self.layer3 != nil)
        {
            self.layer3.removeFromSuperlayer()
            self.layer2.removeFromSuperlayer()
            self.layer1.removeFromSuperlayer()
        }
        
    }

    deinit {
        if (self.layer3 != nil)
        {
            self.layer3.removeFromSuperlayer()
            self.layer2.removeFromSuperlayer()
            self.layer1.removeFromSuperlayer()
        }
    }
    
    func createCGPathForLabels(_ rect: CGRect, alignment: NSTextAlignment) -> CGPath
    {
        let mutablePath : CGMutablePath = CGMutablePath()
        let tiltAngle : CGFloat = 60
        
        //mOffset = (int) ((float) getMeasuredHeight() / (float) Math.tan(Math.toRadians(mTiltAngle)));
        let diagonal : CGFloat = (rect.height / tan(tiltAngle.degreesToRadians))
        
        if (alignment != .right)
        {
            
            mutablePath.move(to: CGPoint(x: rect.origin.x, y: rect.origin.y))
            mutablePath.addLine(to: CGPoint(x: rect.origin.x, y: rect.origin.y + rect.height))
            mutablePath.addLine(to: CGPoint(x: rect.origin.x + rect.width - diagonal, y: rect.origin.y + rect.height))
            mutablePath.addLine(to: CGPoint(x: rect.origin.x + rect.width, y: rect.origin.y))
        }
        else
        {
            mutablePath.move(to: CGPoint(x:rect.origin.x, y: rect.origin.y + rect.height))
            mutablePath.addLine(to: CGPoint(x: rect.origin.x + rect.width, y: rect.origin.y + rect.height))
            mutablePath.addLine(to: CGPoint(x: rect.origin.x + rect.width, y: rect.origin.y ))
            mutablePath.addLine(to: CGPoint(x: rect.origin.x + diagonal, y: rect.origin.y))
        }
        
        return mutablePath
    }
    
    func createRectForLoader(rect: CGRect) -> CGPath
    {
        let mutablePath : CGMutablePath = CGMutablePath()
        let diagonal : CGFloat = (rect.width * 36) / 100
        
        mutablePath.move(to: CGPoint(x:rect.origin.x - diagonal, y: rect.origin.y + rect.height))
        mutablePath.addLine(to: CGPoint(x: rect.origin.x + rect.width + 20, y: rect.origin.y + rect.height))
        mutablePath.addLine(to: CGPoint(x: rect.origin.x + rect.width + 20, y: rect.origin.y ))
        mutablePath.addLine(to: CGPoint(x: rect.origin.x, y: rect.origin.y))
        return mutablePath
    }

   
}
