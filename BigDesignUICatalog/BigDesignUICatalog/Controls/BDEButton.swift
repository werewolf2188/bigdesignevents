//
//  BGMButton.swift
//  BGMCoreUISDKIOS
//
//  Created by Rajiv Dutta on 12/11/16.
//  Copyright © 2016 BBVA. All rights reserved.
//

import Foundation
import UIKit

@IBDesignable
public class BDEButton: UIButton {
    
    //MARK: Init Methods
    
    private var transitionLayer: BDETransitionShapeLayer!
    private let duration : TimeInterval = 0.2
    public var currentBackgroundColor : UIColor? = UIColor.clear
    public var OnDoneAnimation : ((_ sender: BDEButton) -> Void)!
    var snackbar : BDESnackbar!
    
    @IBInspectable public var useTransitionLayer : Bool = true
    @IBInspectable public var showComingSoonAlert : Bool = false
    
    private var normalFontColor : UIColor = UIColor.BDEWhite()
    private var selectedFontColor : UIColor = UIColor.BDEWhite()

    //MARK: Init Methods
    
    required public init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
        self.currentBackgroundColor = self.backgroundColor
        configureLoginButton()
    }
    
    required override public init(frame: CGRect) {
        super.init(frame: frame)
        self.currentBackgroundColor = self.backgroundColor
        configureLoginButton()
    }
    
    func configureLoginButton()
    {
        self.titleLabel?.text = "Login"
        self.setTitleColor(UIColor.white, for: UIControlState.normal)
        self.addTargets()
    }
    
    override public var isEnabled: Bool{
        get {
            return super.isEnabled
        }
        set{
            super.isEnabled = newValue
            checkEnabledAndBackgroundFields()
        }
    }
    
    override public func prepareForInterfaceBuilder() {
        super.prepareForInterfaceBuilder()
        
        checkEnabledAndBackgroundFields()
    }
    
    //MARK:IBInspectable
    
    @IBInspectable public var disabledColor : UIColor? = nil
    
    @IBInspectable public var disabledFontColor : UIColor? = nil
    
    @IBInspectable public var cornerRadius: CGFloat = 0 {
        didSet {
            layer.cornerRadius = cornerRadius
            layer.masksToBounds = cornerRadius > 0
        }
    }
    @IBInspectable public var borderWidth: CGFloat = 0 {
        didSet {
            layer.borderWidth = borderWidth
        }
    }
    
    @IBInspectable public var borderColor: UIColor? = UIColor.clear {
        didSet {
            layer.borderColor = borderColor?.cgColor
        }
    }
    @IBInspectable public var isBackgroundDark: Bool = false{
        didSet{
            checkEnabledAndBackgroundFields()
        }
    }
    
    public func checkEnabledAndBackgroundFields()
    {
        if (!isEnabled)
        {
        if isBackgroundDark {
        self.borderColor = disabledColor != nil ? disabledColor : UIColor.BDE200().withAlphaComponent(0.3)
        self.backgroundColor = disabledColor != nil ? disabledColor : UIColor.BDE200().withAlphaComponent(0.3)
        self.setTitleColor(disabledFontColor != nil ? disabledFontColor : UIColor.BDE400().withAlphaComponent(0.3), for: .disabled)
        }
        else{
        self.borderColor = disabledColor != nil ? disabledColor : UIColor.BDE200()
        self.backgroundColor = disabledColor != nil ? disabledColor : UIColor.BDE200()
        self.setTitleColor(disabledFontColor != nil ? disabledFontColor : UIColor.white, for: .disabled)
        }            }
        else
        {
        self.backgroundColor = self.currentBackgroundColor
        self.borderColor = self.currentBackgroundColor
        self.setTitleColor(normalFontColor, for: .normal)
        self.setTitleColor(selectedFontColor, for: .selected)
        }
        
    }
    
    // MARK: Configure Basic properties
    private func addTargets() {
        
        //------ add target -------
        self.addTarget(self, action: #selector(touchDown), for: UIControlEvents.touchDown)
        self.addTarget(self, action: #selector(touchUpInside), for: UIControlEvents.touchUpInside)
        self.addTarget(self, action: #selector(touchDragExit), for: UIControlEvents.touchDragExit)
        self.addTarget(self, action: #selector(touchDragEnter), for: UIControlEvents.touchDragEnter)
        self.addTarget(self, action: #selector(touchCancel), for: UIControlEvents.touchCancel)
    }
    
    public func touchDown(sender: BDEButton) {
        //self.layer.opacity = 0.4
        if (self.transitionLayer != nil && self.isEnabled){
            self.transitionLayer.removeAllAnimations()
            self.transitionLayer.removeFromSuperlayer()
            self.transitionLayer = nil
        }
    }
    
    public func touchUpInside(sender: BDEButton) {
        if (self.isEnabled && self.useTransitionLayer)
        {
            animationTransition()
        }
        if (self.showComingSoonAlert)
        {
            comingSoonAlert()
        }
    }
    
    private func comingSoonAlert()
    {
        DispatchQueue.main.async {
            self.showComingSoon()
        }

    }
    
    public func touchDragExit(sender: BDEButton) {
        //self.layer.opacity = 1.0
    }
    
    public func touchDragEnter(sender: BDEButton) {
        //self.layer.opacity = 0.4
    }
    
    public func touchCancel(sender: BDEButton) {
        //self.layer.opacity = 1.0
    }
    
    @IBAction func loginClicked(sender: UIButton) {
        buttonAnimation()
    }
    
    private func animationTransition()
    {
        //1
        let color: UIColor = self.backgroundColor != nil && self.backgroundColor?.darker(amount: 0.25) != nil ? (self.backgroundColor?.darker(amount: 0.25))! : UIColor.clear
        
        self.transitionLayer = BDETransitionShapeLayer(bounds: self.bounds, fillColor: (color.cgColor), duration: self.duration)
        //2
        self.transitionLayer.OnDoneAnimation =
            { (sender: Any?) in
                //print("lol")
                if (self.OnDoneAnimation != nil)
                {
                    self.OnDoneAnimation(self)
                }
        }
        //3
        self.layer.addSublayer(self.transitionLayer)
    }
    
    
    public override func setTitleColor(_ color: UIColor?, for state: UIControlState) {
        switch state {
        case UIControlState.normal:
            normalFontColor = color!
        case UIControlState.disabled:
            disabledFontColor = color
        case UIControlState.selected:
            selectedFontColor = color!
        default:
            break
        }
        super.setTitleColor(color, for: state)
    }
    
    private func buttonAnimation()
    {
        UIView.animate(withDuration:1.0, delay: 0.0, options: UIViewAnimationOptions.layoutSubviews, animations: {
            self.transform = CGAffineTransform(scaleX: 0.20, y: 1.0)
        }) { (true) in
            self.isHidden = true
        }
    }

}
