//
//  ProductTableView.swift
//  BGMAccountsUIIos
//
//  Created by Enrique Ricalde on 8/8/17.
//  Copyright © 2017 BBVA. All rights reserved.
//

import UIKit
open class BDETableView: UITableView, UITableViewDataSource, UITableViewDelegate {

    public var height: CGFloat!
    {
        get
        {
            var _height : CGFloat = 0
            for item in self.products
            {
                _height = _height + self.getHeightForItem(object: item)
            }
            return _height + BDETableViewHeights.header
        }
    }
    public var productSection : Double = 0
    open var products : [AnyObject]!
    open var semiviews : [BDECardView] = []
    public var dashboard : UIViewController!
    
    public init()
    {
        super.init(frame: CGRect(), style: .plain)
        self.separatorColor = UIColor.BDE200()
        self.dataSource = self
        self.delegate = self
        self.backgroundColor = UIColor.blue
        self.isUserInteractionEnabled = true
        //self.tableFooterView?.isHidden = true
        self.isScrollEnabled = false
    }
    
    required public init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    open func registerCells()
    {
        
    }
    
    open func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    open func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return products.count
    }
    
    open func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell : UITableViewCell = UITableViewCell()
        
        if (indexPath.row < self.products.count - 1)
        {
            cell.separatorInset = UIEdgeInsets.zero
        }
        else
        {
            cell.separatorInset = UIEdgeInsets(top: 0, left: 1000, bottom: 0, right: 0)
        }
        cell.selectionStyle = .none
        return cell
    }
    
    open func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 0
    }
    
//    open func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
//        return 1
//    }
    
    open func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return BDETableViewHeights.header
    }
    
    open func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        return UIView()
    }
    
    open func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let cell : UITableViewCell? = self.cellForRow(at: indexPath)
//        //selectedView.layer.addSublayer(transitionLayer)
        if (cell != nil)
        {
            
            let transitionLayer: BDETransitionShapeLayer = BDETransitionShapeLayer(bounds: CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: cell!.bounds.height), fillColor: UIColor.BDE300().cgColor , duration: 0.2)
            transitionLayer.OnDoneAnimation = { (sender: Any?) in
                
                
                self.indexPressed(index: indexPath.row)
            }
            if (cell!.viewWithTag(3) != nil)
            {
                cell!.viewWithTag(3)!.layer.addSublayer(transitionLayer)
            }
        }
        
    }
    
    open func indexPressed(index : Int)
    {
        
    }
    
    open func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath)
    {
        
    }
    
    open func tableView(_ tableView: UITableView, willDisplayHeaderView view: UIView, forSection section: Int)
    {
        
    }
    
    open func getHeightForItem(object: AnyObject) -> CGFloat
    {
        return 0
    }
    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */

}
