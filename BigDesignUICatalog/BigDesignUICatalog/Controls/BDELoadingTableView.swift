//
//  BGMLoadingTableView.swift
//  BGMAccountsUIIos
//
//  Created by Enrique Ricalde on 8/8/17.
//  Copyright © 2017 BBVA. All rights reserved.
//

import UIKit
public class BDELoadingTableView: BDETableView {

 
    private var animate : Bool = true
    
    public init(animate : Bool = true){
        super.init()
        self.animate = animate
    }
    
    required public init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */

    override public func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        
        return BDETableViewHeights.header
    }
    
    override public func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return getHeightForItem(object: self.products[indexPath.row])
    }
    
    override public func getHeightForItem(object: AnyObject) -> CGFloat
    {
        return BDETableViewHeights.loadingView1
    }
    
    override public func tableView(_ tableView: UITableView, willDisplayHeaderView view: UIView, forSection section: Int) {
        let cardView : BDEHeaderCardView = (Bundle(for: BDEHeaderCardView.self).loadNibNamed("BDEHeaderCardView", owner: nil, options: nil)?.first as? BDEHeaderCardView)!
        cardView.titleLabel.text = ""
        var frame : CGRect = view.frame
        frame.origin = CGPoint(x: 0, y: 0)
        cardView.frame = frame
        view.addSubview(cardView)

    }
    
    override public func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        var cardView : BDECardView
        cardView = (Bundle(for: BDELoadingTableView.self).loadNibNamed("BDELoadingCardView", owner: nil, options: nil)?.first as? BDECardView)!
        var frame : CGRect = cell.frame
        frame.origin = CGPoint(x: 0, y: 0)
        cardView.frame = frame
        cardView.tag = 3
        cell.contentView.addSubview(cardView)
        if (animate)
        {
            cardView.row = TimeInterval(self.products.count) - (TimeInterval(indexPath.row))
        }
       
    }
}
