//
//  BGMTransitionShapeLayer.swift
//  BGMCoreUISDKIOS
//
//  Created by Enrique Ricalde on 1/30/17.
//  Copyright © 2017 BBVA. All rights reserved.
//

import UIKit

public class BDETransitionShapeLayer: CAShapeLayer, CAAnimationDelegate {
    public var OnDoneAnimation : ((_ sender: Any?) -> Void)!
    
     public init(bounds: CGRect, fillColor: CGColor, duration: TimeInterval) {
        super.init()
        
        let pathInit: CGMutablePath = CGMutablePath()
        let pathFinal : CGMutablePath = CGMutablePath()
        let extra : CGFloat = 13
        
        pathInit.move(to: CGPoint(x: bounds.midX + extra - 1, y: 0))
        pathInit.addLine(to: CGPoint(x: bounds.midX + extra + 2, y: 0))
        pathInit.addLine(to: CGPoint(x: bounds.midX - extra + 2, y: bounds.size.height))
        pathInit.addLine(to: CGPoint(x: bounds.midX - extra - 1, y: bounds.size.height))
        
        pathFinal.move(to: CGPoint(x: 0, y: 0))
        pathFinal.addLine(to: CGPoint(x: bounds.maxX + extra, y: 0))
        pathFinal.addLine(to: CGPoint(x: bounds.maxX, y: bounds.size.height))
        pathFinal.addLine(to: CGPoint(x: bounds.minX - extra, y: bounds.size.height))
        
        self.path = pathInit
        self.fillColor = fillColor //this needs to change or not
        self.opacity = 0.6 //need to define this
        
        self.prepareAnimation(keyPath: "path", fromValue: nil, toValue: pathFinal, beginTime: CACurrentMediaTime(), duration: duration, delegate: nil, key: nil)
        
        
        self.prepareAnimation(keyPath: "opacity", fromValue: nil, toValue: 0, beginTime: Double(CACurrentMediaTime()), duration: duration, delegate: self, key: "finish")
        
    }
    
    required public init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
    }
    
    public func animationDidStop(_ anim: CAAnimation, finished flag: Bool)
    {
        if (flag)
        {
            
            if (anim == self.animation(forKey: "finish"))
            {
                //print("done")
                self.removeAllAnimations()
                self.removeFromSuperlayer()
                
                if (self.OnDoneAnimation != nil)
                {
                    self.OnDoneAnimation(self)
                }
            }
        }
    }

}
