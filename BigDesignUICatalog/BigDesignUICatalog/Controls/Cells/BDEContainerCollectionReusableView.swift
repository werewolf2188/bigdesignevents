//
//  BGMContainerCollectionReusableView.swift
//  BGMAccountsUIIos
//
//  Created by Enrique Ricalde on 8/10/17.
//  Copyright © 2017 BBVA. All rights reserved.
//

import UIKit

public class BDEContainerCollectionReusableView: UICollectionReusableView {

    override public func awakeFromNib() {
        super.awakeFromNib()
        self.backgroundColor = UIColor.BDE100()
        // Initialization code
    }
    
}
