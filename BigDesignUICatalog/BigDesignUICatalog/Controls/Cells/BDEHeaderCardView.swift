//
//  BGMHeaderCardView.swift
//  BGMAccountsUIIos
//
//  Created by Enrique Ricalde on 2/7/17.
//  Copyright © 2017 BBVA. All rights reserved.
//

import UIKit
@IBDesignable
class BDEHeaderCardView: BDECardView {
    
    @IBOutlet weak var titleLabel: UILabel!
    
    
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        //Change the tags to constants later
        self.titleLabel = self.viewWithTag(1) as! UILabel!
    }
    
    override public init(frame: CGRect) {
        super.init(frame: frame)
    }
}
