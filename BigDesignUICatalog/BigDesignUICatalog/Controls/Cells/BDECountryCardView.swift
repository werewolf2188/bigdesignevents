//
//  BGMAccountsCardView.swift
//  BGMAccountsUIIos
//
//  Created by Enrique Ricalde on 2/6/17.
//  Copyright © 2017 BBVA. All rights reserved.
//

import UIKit
import BigDesignManager

@IBDesignable
class BDECountryCardView: BDECardView {

    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var amount: UILabel!
    @IBOutlet weak var number: UILabel!
    
    //@IBOutlet weak var date: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()

        // Initialization code
        emptyCellContents()
    }
    
    override public init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
       
        self.name = self.viewWithTag(1) as! UILabel!
        self.amount = self.viewWithTag(2) as! UILabel!
        self.number = self.viewWithTag(3) as! UILabel!
    }
    
    
    func populateCellFor(country : Country ) {
        
        emptyCellContents()
        
        //Alias
        if let name = country.name
        {
            self.name.text = "Country: \(name)"
        }
        if let capital = country.capital
        {
            self.number.text = "Capital: \(capital)"
        }
        //LAST Digits.
        if let population = country.population
        {
            let formatter : NumberFormatter = NumberFormatter()
            formatter.maximumFractionDigits = 0
            formatter.numberStyle = .decimal
            self.amount.text = "Population: \(formatter.string(from: NSNumber(value: population))!)"
        }
                
    }
    
    func emptyCellContents() {
        
        self.name.text = ""
        self.number.text = ""
        self.amount.text = ""
    }
}
